**Run php code in html|htm for nginx**  
  
    location ~ \.(html|htm) {

        include /etc/nginx/fastcgi_params;

        fastcgi_pass  unix:/var/run/php5-fpm.sock;

        fastcgi_index (index.html|index.htm);

        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;

    }
    
**========================================================================**


**Run php code in html|htm for apache need add to .htaccess**   

    <FilesMatch "\.(htm|html)$">
        SetHandler application/x-httpd-php
    </FilesMatch>
    