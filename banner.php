<?php

use src\Mysql;
use src\Visitors;

require "autoload.php";
spl_autoload_register('autoload');


$database = new Mysql();
$database->connect();


function index ($database) {
    $visitor = new Visitors($database);
    $userAgent = $_SERVER['HTTP_USER_AGENT'];
    $url = !empty($_GET) ? stristr($_SERVER['REQUEST_URI'], '?', true) : $_SERVER['REQUEST_URI'];
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }


    $data = [
        ':ip_address'   => $ip,
        ':user_agent'   => htmlspecialchars($userAgent),
        ':page_url'     => htmlspecialchars($url)
    ];

    $visitor->checkNewVisitor($data);

    if ($url === '/index1.html') {
        return 'images/72975551.webp';
    }
    else if ($url === '/index2.html')  {
        return 'images/photo-1541963463532-d68292c34b19.jfif';
    }
}

echo index($database);