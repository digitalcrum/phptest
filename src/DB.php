<?php

namespace src;

interface DB
{
    public function connect ($dsn, $user='', $password = '');

    public function prepare($sql);
}