<?php

namespace src;

class Mysql implements DB
{
    protected $db = null;

    public function connect($dsn = 'mysql:host=localhost;dbname=php_test', $user = 'root', $password = 'root')
    {
        try {
            $this->db = new \PDO($dsn, $user, $password);
        }
        catch (\PDOException $e) {
            die($e->getMessage());
        }
    }

    public function prepare ($sql)
    {
        return $this->db->prepare($sql);
    }
}