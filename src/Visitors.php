<?php

namespace src;

class Visitors
{
    private $db = null;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function checkNewVisitor ($data)
    {
        $visitors = $this->getVisitor($data);
        if (!empty($visitors)) {
            $sql = 'UPDATE visitors SET views_count=views_count+1 WHERE ip_address=:ip_address AND user_agent=:user_agent AND page_url=:page_url';
        }
        else {
            $sql = 'INSERT INTO visitors (ip_address, user_agent, page_url) VALUES (:ip_address, :user_agent, :page_url)';
        }

        $sqlQuery = $this->db->prepare($sql);
        try {
            $sqlQuery->execute($data);
        }
        catch (\PDOException $e) {
            die($e->getMessage());
        }

        return true;
    }

    public function getVisitor ($data)
    {
        $sql = 'SELECT ip_address FROM visitors WHERE ip_address=:ip_address AND user_agent=:user_agent AND page_url=:page_url';
        $visitors = $this->db->prepare($sql);
        try {
            $visitors->execute($data);
        }
        catch (\PDOException $e) {
            die($e->getMessage());
        }

        return $visitors->fetch();
    }
}